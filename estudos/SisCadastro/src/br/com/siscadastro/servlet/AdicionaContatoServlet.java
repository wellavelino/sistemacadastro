package br.com.siscadastro.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdicionaContatoServlet extends HttpServlet
{

	private static final long	serialVersionUID	= 1L;
	private HttpSession			sessao;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();
		
		sessao = request.getSession();
		String usuario = (String) sessao.getAttribute("usuario");
		
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String endereco = request.getParameter("endereco");
		String dataConverter = request.getParameter("dataNascimento");
		Calendar dataNascimento = null;
		try
		{
			Date date = new SimpleDateFormat("dd/mm/yyyy").parse(dataConverter);
			dataNascimento = Calendar.getInstance();
			dataNascimento.setTime(date);

		} catch (ParseException e)
		{
			out.println("Erro na convers�o da data");
			return;
		}
	}
}

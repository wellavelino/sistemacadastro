package br.com.siscadastro.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class InicializadorServlet extends HttpServlet
{

	private static final long	serialVersionUID	= 1L;
	private String				login_usuario		= "Wellington";
	private String				senha_usuario		= "123";
	String						pagina;
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		HttpSession sessao = request.getSession();
		String usuario = request.getParameter("usuario");
		String senha = request.getParameter("senha");

		if (usuario == login_usuario && senha == senha_usuario)
		{
			sessao.setAttribute("usuario", usuario);
			sessao.setAttribute("senha", senha);

			pagina = "/adiciona-contato.jsp";
		} else
		{
			request.setAttribute("mensagem", "Usu�rio ou Senha Inv�lidos!");
			pagina = "/login.jsp";
		}

		response.sendRedirect(pagina);

	}
}

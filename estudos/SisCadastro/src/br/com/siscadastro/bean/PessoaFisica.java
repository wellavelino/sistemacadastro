package br.com.siscadastro.bean;

import java.util.List;

public class PessoaFisica
{
	private String				nome;
	private String				sobrenome;
	private int					idade;
	private double				salario;
	private String				dataNascimento;
	private Acesso				acesso;
	private List<Dependentes>	dependentes;

	public PessoaFisica(String nome, String sobrenome, int idade, double salario, String dataNascimento, List<Dependentes> dependentes)
	{
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.idade = idade;
		this.salario = salario;
		this.dataNascimento = dataNascimento;
		this.dependentes = dependentes;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public String getSobrenome()
	{
		return sobrenome;
	}

	public void setSobrenome(String sobrenome)
	{
		this.sobrenome = sobrenome;
	}

	public int getIdade()
	{
		return idade;
	}

	public void setIdade(int idade)
	{
		this.idade = idade;
	}

	public double getSalario()
	{
		return salario;
	}

	public void setSalario(double salario)
	{
		this.salario = salario;
	}

	public String getDataNascimento()
	{
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento)
	{
		this.dataNascimento = dataNascimento;
	}

	public Acesso getAcesso()
	{
		return acesso;
	}

	public void setAcesso(Acesso acesso)
	{
		this.acesso = acesso;
	}

	public List<Dependentes> getDependentes()
	{
		return dependentes;
	}

	public void setDependentes(List<Dependentes> dependentes)
	{
		this.dependentes = dependentes;
	}

}
